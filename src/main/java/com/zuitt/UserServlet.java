package com.zuitt;

import java.io.IOException;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;

	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" UserServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) 
			throws IOException, ServletException {
		
		ServletContext srvContext = getServletContext();
		HttpSession session = req.getSession();
		
		
		String fname = req.getParameter("fname");
		System.getProperties().put("fname", fname);
		
		String lname = req.getParameter("lname");		
		session.setAttribute("lname", lname);
		
		String email = req.getParameter("email");
		srvContext.setAttribute("mail", email);
		
		String contact = req.getParameter("contact");
		res.sendRedirect("details?contact="+contact);

	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" UserServlet has been destroyed. ");
		System.out.println("******************************************");
	}
}
